package animals;

import customExceptions.WrongFoodException;
import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {

    public void eat(Food food) throws WrongFoodException{
        if(!(food instanceof Meat)){
            throw new WrongFoodException("������� ���� ����!");
        } else {
            System.out.println("������ " + getName() + " ���� ���� " + food.getName());
            this.setSatiety(getSatiety() + 2);
        }
    }
}

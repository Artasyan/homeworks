package animals;

import animals.interfaces.Fly;
import animals.interfaces.Run;
import animals.interfaces.Voice;
import avairy.SizeOfAviary;

public class Eagle extends Carnivorous implements Fly, Voice, Run {

    public Eagle(String name) {
        this.name = name;
        setRequiredSize(SizeOfAviary.BIG);
    }

    public void fly() {
        if(isHungry()){
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName() + " ��������!");
        setSatiety(getSatiety() - 5);
    }

    public void run() {
        if(isHungry()){
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName()+ " ������");
        setSatiety(getSatiety() - 2);
    }


    public String voice() {
        return "����� ����)";
    }
}

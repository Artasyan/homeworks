package animals;

import customExceptions.WrongFoodException;
import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal{

    public void eat(Food food) throws WrongFoodException{
        if(!(food instanceof Grass)){
            throw new WrongFoodException("��������� ��� ������ �����!");
        } else {
            System.out.println("���������� " + getName() + " ����� " + food.getName());
            this.setSatiety(getSatiety() + 3);
        }
    }
}

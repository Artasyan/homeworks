package animals;

import animals.interfaces.Run;
import animals.interfaces.Voice;
import avairy.SizeOfAviary;

public class Goat extends Herbivore implements Run, Voice {

    public Goat(String name) {
        this.name = name;
        setRequiredSize(SizeOfAviary.BIG);
    }

    public void run() {
        if(isHungry()){
            System.out.println("����� " + getName() + "����� ����");
            return;
        }
        System.out.println("����� " + getName()+ " ������");
        setSatiety(getSatiety() - 2);
    }

    public String voice() {
        return "�����-�-�-�";
    }
}

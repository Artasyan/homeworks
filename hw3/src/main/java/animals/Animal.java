package animals;

import avairy.SizeOfAviary;
import customExceptions.WrongFoodException;
import food.Food;

import java.util.Objects;

public abstract class Animal {
    protected String name;
    protected int satiety = 10;
    protected SizeOfAviary requiredSize;

    public abstract void eat(Food food) throws WrongFoodException;

    public SizeOfAviary getRequiredSize() {
        return requiredSize;
    }

    public void setRequiredSize(SizeOfAviary requiredSize) {
        this.requiredSize = requiredSize;
    }

    public boolean isHungry() {
        return satiety <= 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) return false;
        return Objects.equals(this.name, ((Animal) o).name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", satiety=" + satiety +
                ", requiredSize=" + requiredSize +
                '}';
    }
}

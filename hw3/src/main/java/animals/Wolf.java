package animals;

import animals.interfaces.Run;
import animals.interfaces.Voice;
import avairy.SizeOfAviary;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf(String name) {
        this.name = name;
        setRequiredSize(SizeOfAviary.BIG);
    }

    public void run() {
        if (isHungry()) {
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName() + " �����");
        setSatiety(getSatiety() - 3);

    }

    public String voice() {
        return "��������";
    }
}

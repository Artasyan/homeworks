package animals;

import animals.interfaces.Swim;
import avairy.SizeOfAviary;

public class CarnivoreFish extends Carnivorous implements Swim {

    public CarnivoreFish(String name) {
        this.name = name;
        setRequiredSize(SizeOfAviary.SMALL);
    }

    public void swim() {
        if (isHungry()) {
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("������ ���� " + getName() + " �������");
        setSatiety(getSatiety() - 2);
    }
}

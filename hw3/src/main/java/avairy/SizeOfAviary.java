package avairy;

public enum SizeOfAviary {
    SMALL(1),
    MEDIUM(2),
    BIG(3),
    HUGE(4);

    private final int size;

    SizeOfAviary(int size) {
        this.size = size;
    }

    public int getSize(){
        return size;
    }
}

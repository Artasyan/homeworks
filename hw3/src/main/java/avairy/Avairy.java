package avairy;

import animals.Animal;

import java.util.HashSet;
import java.util.Set;

public class Avairy<T extends Animal> {
    private Set<T> set = new HashSet<>();
    private SizeOfAviary size;

    public Avairy(SizeOfAviary size) {
        this.size = size;
    }

    public void add(T animal) {
        if(animal.getRequiredSize().getSize() <= size.getSize()){
            set.add(animal);
            return;
        }
        System.out.println("������� ��������� ������ ��� ��������� " + animal.getClass().getSimpleName());
        System.out.println("������������� ������ ��� " + animal.getClass().getSimpleName() + ": " + animal.getRequiredSize());
    }

    public void remove(T animal) {
        set.remove(animal);
    }

    public T get(String name) {
        for (T animal : set) {
            if (name.equals(animal.getName())) {
                return animal;
            }
        }
        return null;
    }
}

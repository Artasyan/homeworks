import animals.*;
import avairy.Avairy;
import avairy.SizeOfAviary;
import customExceptions.WrongFoodException;
import food.Meat;


public class Zoo {
    public static void main(String[] args) throws WrongFoodException {
        new Duck("������");
        new CarnivoreFish("����");

        Avairy<Herbivore> herbivoreAvairy = new Avairy<>(SizeOfAviary.HUGE);
        herbivoreAvairy.add(new Horse("Spirit"));
        herbivoreAvairy.add(new Duck("mcDonald"));

        Avairy<Carnivorous> carnivorousAvairy = new Avairy<>(SizeOfAviary.BIG);
        carnivorousAvairy.add(new Wolf("Volk"));
        carnivorousAvairy.add(new Eagle("Orel"));
        carnivorousAvairy.add(new CarnivoreFish("ryba"));

        Worker worker = new Worker();
        worker.feed(new Horse("����"), new Meat("chicken"));

    }
}

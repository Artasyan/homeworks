package model;

public class Kotik {
    private int satiety = 10;
    private String name;
    private static int countOfKotiks = 0;
    private int weight;
    private int prettiness;
    private String meow;

    public Kotik() {
        countOfKotiks++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.name = name;
        this.weight = weight;
        this.prettiness = prettiness;
        this.meow = meow;
        countOfKotiks++;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        setPrettiness(prettiness);
        setName(name);
        setWeight(weight);
        setMeow(meow);
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    private boolean isHungry() {
        if (satiety <= 0) {
            System.out.println("Котик " + name + " хочет кушать");
            return true;
        }
        return false;
    }

    public boolean eat(int countSatiety) {
        return eat(countSatiety, "кошачий корм");
    }

    public boolean eat(int countSatiety, String food) {
        this.satiety += countSatiety;
        System.out.println("Котик скушал " + food + " в количестве: " + countSatiety);
        return true;
    }

    public boolean eat() {
        return eat(3);
    }

    public boolean play() {
        if (isHungry()) {
            return false;
        }
        System.out.println("Котик " + name + " играет");
        satiety -= 2;
        return true;
    }

    public boolean sleep() {
        if (isHungry()) {
            return false;
        }
        System.out.println("Котик " + name + " спит");
        satiety -= 3;
        return true;
    }

    public boolean chaseMouse() {
        if (isHungry()) {
            return false;
        }
        System.out.println("Котик " + name + " ловит мышь");
        satiety -= 2;
        return true;
    }

    public boolean wash() {
        if (isHungry()) {
            return false;
        }
        System.out.println("Котик " + name + " умывается");
        satiety--;
        return true;
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            int actionIndex = (int) (Math.random() * 5);
            switch (actionIndex) {
                case 0:
                    eat();
                    break;
                case 1:
                    if (!play()) {
                        eat();
                    }
                    break;
                case 2:
                    if (!sleep()) {
                        eat();
                    }
                    break;
                case 3:
                    if (!chaseMouse()) {
                        eat();
                    }
                    break;
                case 4:
                    if (!wash()) {
                        eat();
                    }
            }
        }
    }

    public static int getCountOfKotiks() {
        return countOfKotiks;
    }
}

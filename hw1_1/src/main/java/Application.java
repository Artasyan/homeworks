import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik barsik = new Kotik(999, "Барсик", 5, "мяу");
        Kotik murzik = new Kotik();
        murzik.setKotik(115, "Мурзик", 6, "мяяяяяяяууу");
        murzik.liveAnotherDay();
        System.out.println("Кот " + murzik.getName() + " весит " + murzik.getWeight() + " кило");
        System.out.println(barsik.getMeow().equals(murzik.getMeow()));
        System.out.println(Kotik.getCountOfKotiks());
    }
}

import animals.*;
import animals.interfaces.Swim;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker();

        Wolf wolf = new Wolf("�����");
        CarnivoreFish fish = new CarnivoreFish("����");
        Horse horse = new Horse("������");
        Duck duck = new Duck("�������");
        Eagle eagle = new Eagle("�����");
        Goat goat = new Goat("��������");

        Food grass = new Grass("����");
        Food meat = new Meat("������");

        worker.feed(wolf, meat);
        worker.feed(fish, grass);
        worker.feed(goat, grass);

        worker.getVoice(goat);
        worker.getVoice(eagle);
        worker.getVoice(duck);

        Swim[] pond = new Swim[5];
        pond[0] = duck;
        pond[1] = new Duck("������");
        pond[2] = fish;
        pond[3] = new CarnivoreFish("����");
        pond[4] = new CarnivoreFish("�������");

        for (Swim swim : pond) {
            swim.swim();
        }

        System.out.println("������");
        for (int i = 0; i < 10; i++) {
            horse.run();
        }
    }
}

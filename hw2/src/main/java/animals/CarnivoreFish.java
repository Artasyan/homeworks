package animals;

import animals.interfaces.Swim;

public class CarnivoreFish extends Carnivorous implements Swim {
    @Override
    public void swim() {
        if(isHungry()){
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("������ ���� " + getName() + " �������");
        setSatiety(getSatiety() - 2);
    }

    public CarnivoreFish(String name) {
        super(name);
    }
}

package animals;

import animals.interfaces.Run;
import animals.interfaces.Voice;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf(String name) {
        this.name = name;
    }

    public void run() {
        if (isHungry()) {
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName() + " �����");
        setSatiety(getSatiety() - 3);

    }

    public String voice() {
        return "��������";
    }
}

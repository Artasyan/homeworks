package animals;

import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    public void eat(Food food) {
        if (!(food instanceof Grass)) {
            System.out.println("���������� �������� ����������!");
        } else {
            System.out.println("���������� " + getName() + " ����� " + food.getName());
            this.setSatiety(getSatiety() + 3);
        }
    }
}

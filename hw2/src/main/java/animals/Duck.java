package animals;

import animals.interfaces.*;


public class Duck extends Herbivore implements Fly, Run, Swim, Voice {
    public Duck(String name) {
        this.name = name;
    }

    public void fly() {
        if (isHungry()) {
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName() + " ��������!");
        setSatiety(getSatiety() - 5);
    }


    public void run() {
        if (isHungry()) {
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName() + " ������");
        setSatiety(getSatiety() - 2);
    }


    public void swim() {
        if (isHungry()) {
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName() + " �������");
        setSatiety(getSatiety() - 4);
    }


    public String voice() {
        return "���-���";
    }

}

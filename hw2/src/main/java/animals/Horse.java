package animals;

import animals.interfaces.Run;
import animals.interfaces.Voice;

public class Horse extends Herbivore implements Run, Voice {
    public Horse(String name) {
        this.name = name;
    }

    public void run() {
        if(isHungry()){
            System.out.println("���� " + getName() + " ����� ����");
            return;
        }
        System.out.println("���� " + getName() + " �����");
        setSatiety(getSatiety() - 3);
    }

    public String voice() {
        return "���-��";
    }
}

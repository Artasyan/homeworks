package animals;

import food.Food;

public abstract class Animal {
    protected String name;
    protected int satiety = 10;

    public abstract void eat(Food food);

    public boolean isHungry(){
        return satiety <= 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }
}

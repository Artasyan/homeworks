package food;

public class Meat extends Food {
    public Meat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
